#!bin/bash

#ETENV_SETTED promise run this only once.

if [ -z $SETENV_SETTED ]; then
        SETENV_SETTED="true"

        APP_NAME=bsweb
        APP_HOME=/home/admin/${APP_NAME}
		


        export JAVA_HOME=/opt/jdk1.8.0_162/
        export PATH=${PATH}:${JAVA_HOME}/bin
        ulimit -c unlimited

        #init params
        export LANG=zh_CN.UTF-8
        export JAVA_FILE_ENCODING=UTF-8
        export NLS_LANG=AMERICAN_AMERICA.ZHS16GBK
		export CPU_COUNT="$(grep -c 'cpu[0-9][0-9]*' /proc/stat)"

        mkdir -p "$APP_HOME"/.default

        export SERVICE_PID=$APP_HOME/.default/${APP_NAME}.pid
        export SERVICE_OUT=$APP_HOME/logs/service_stdout.log
        export MIDDLEWARE_LOGS="${HOME}/logs"
        export MIDDLEWARE_SNAPSHOTS="${HOME}/snapshots"

        # Define the java.io.tmpdir to use for Service
        SERVICE_TMPDIR="${APP_HOME}"/.default/temp

        SERVICE_OPTS="${SERVICE_OPTS} -server"

        let memTotal=`cat /proc/meminfo | grep MemTotal | awk '{printf "%d", $2/1024 }'`
        echo "INFO: OS total memory: "$memTotal"M"
        #if os memory <= 2G
		
	SERVICE_OPTS="${SERVICE_OPTS} -Xms3g  -Xmx3g"
        SERVICE_OPTS="${SERVICE_OPTS} -Xmn1g"
        #if [ $memTotal -le 2048 ]; then
        #  SERVICE_OPTS="${SERVICE_OPTS} -Xms256m -Xmx528m"
        #  SERVICE_OPTS="${SERVICE_OPTS} -Xmn268m"
        #else
        #  SERVICE_OPTS="${SERVICE_OPTS} -Xms2g -Xmx2g"
        #  SERVICE_OPTS="${SERVICE_OPTS} -Xmn1g"
        #fi


        # SERVICE_OPTS="${SERVICE_OPTS} -XX:PermSize=256m -XX:MaxPermSize=512m"
	SERVICE_OPTS="${SERVICE_OPTS} -XX:MetaspaceSize=256m -XX:MaxMetaspaceSize=512m"
        SERVICE_OPTS="${SERVICE_OPTS} -XX:MaxDirectMemorySize=512m"
        SERVICE_OPTS="${SERVICE_OPTS} -XX:SurvivorRatio=10"
        SERVICE_OPTS="${SERVICE_OPTS} -XX:+UseConcMarkSweepGC -XX:+UseCMSCompactAtFullCollection -XX:CMSMaxAbortablePrecleanTime=5000"
        SERVICE_OPTS="${SERVICE_OPTS} -XX:+CMSClassUnloadingEnabled -XX:CMSInitiatingOccupancyFraction=80 -XX:+UseCMSInitiatingOccupancyOnly"
        #SERVICE_OPTS="${SERVICE_OPTS} -XX:+ExplicitGCInvokesConcurrent -Dsun.rmi.dgc.server.gcInterval=2592000000 -Dsun.rmi.dgc.client.gcInterval=2592000000"
        SERVICE_OPTS="${SERVICE_OPTS} -XX:ParallelGCThreads=${CPU_COUNT}"
        #SERVICE_OPTS="${SERVICE_OPTS} -Xloggc:${MIDDLEWARE_LOGS}/gc.log -XX:+PrintGCDetails -XX:+PrintGCDateStamps"
        #SERVICE_OPTS="${SERVICE_OPTS} -XX:+HeapDumpOnOutOfMemoryError -XX:HeapDumpPath=${MIDDLEWARE_LOGS}/java.hprof"
        SERVICE_OPTS="${SERVICE_OPTS} -Xloggc:${APP_HOME}/logs/gc.log -XX:+PrintGCDetails -XX:+PrintGCDateStamps"
        SERVICE_OPTS="${SERVICE_OPTS} -XX:+HeapDumpOnOutOfMemoryError -XX:HeapDumpPath=${APP_HOME}/logs/java.hprof"
        SERVICE_OPTS="${SERVICE_OPTS} -Djava.awt.headless=true"
        SERVICE_OPTS="${SERVICE_OPTS} -Dsun.net.client.defaultConnectTimeout=10000"
        SERVICE_OPTS="${SERVICE_OPTS} -Dsun.net.client.defaultReadTimeout=30000"
        #SERVICE_OPTS="${SERVICE_OPTS} -DJM.LOG.PATH=${MIDDLEWARE_LOGS}"
        #SERVICE_OPTS="${SERVICE_OPTS} -DJM.SNAPSHOT.PATH=${MIDDLEWARE_SNAPSHOTS}"
        SERVICE_OPTS="${SERVICE_OPTS} -DJM.LOG.PATH=${APP_HOME}/logs"
        SERVICE_OPTS="${SERVICE_OPTS} -DJM.SNAPSHOT.PATH=${APP_HOME}/logs/snapshots"
        SERVICE_OPTS="${SERVICE_OPTS} -Dfile.encoding=${JAVA_FILE_ENCODING}"
        SERVICE_OPTS="${SERVICE_OPTS} -Dhsf.publish.delayed=true"
        SERVICE_OPTS="${SERVICE_OPTS} -Dproject.name=${APP_NAME}"
        #SERVICE_OPTS="${SERVICE_OPTS} -Dpandora.boot.wait=true -Dlog4j.defaultInitOverride=true"

        # debug opts

        # jpda options
        test -z "$JPDA_ENABLE" && JPDA_ENABLE=0
        test -z "$JPDA_ADDRESS" && export JPDA_ADDRESS=8000
        test -z "$JPDA_SUSPEND" && export JPDA_SUSPEND=n

        if [ "$JPDA_ENABLE" -eq 1 ]; then
                if [ -z "$JPDA_TRANSPORT" ]; then
                        JPDA_TRANSPORT="dt_socket"
                fi
                if [ -z "$JPDA_ADDRESS" ]; then
                        JPDA_ADDRESS="8000"
                fi
                if [ -z "$JPDA_SUSPEND" ]; then
                        JPDA_SUSPEND="n"
                fi
                if [ -z "$JPDA_OPTS" ]; then
                        JPDA_OPTS="-agentlib:jdwp=transport=$JPDA_TRANSPORT,address=$JPDA_ADDRESS,server=y,suspend=$JPDA_SUSPEND"
                fi
                SERVICE_OPTS="$SERVICE_OPTS $JPDA_OPTS"
        fi
		
        export SERVICE_OPTS

fi
